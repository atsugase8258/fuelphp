<?php
class Controller_Api extends Controller_Rest {
    public function post_vote() {
        // ログインされているか
        if ( Auth::check()) {
        $id = Input::post('id');
            $image = Model_Image::find($id);
            $image->votes = $image->votes + 1;
            $image->save();
        // 投票処理
        }
        return $this->response( 
            array('message' => '処理終了')
        );
    }
}
