<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>一覧画面</title>
    <style>
        body { margin:40px; }
    </style>
<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous">
</script>
<script>
    $(function(){
        $('input.vote').on('click',function(){
            var id =$(this).data('id');
            $.ajax({
                url:"<?php echo Uri::create('api/vote.json') ?>",
                type: "POST",
                data: {id:id},
                dataType: "json"
            }).done(function(data){
                alert(data.message);
                location.reload();
            }).fail(function(data){
                alert('処理はスタッフが美味しくいただきました。')
            });
        });
    });
</script>
<style>
    
body{
    font-family: YuGothic, 'Yu Gothic' ;
}
    
a{
    text-decoration:none;
}
    
a:hover{
    color:red;
}

h2{
    color:#036;
}
    
li{
    list-style:none;
}

.vote{
    background-color:#fff;
    border:2px solid #036;
    border-radius:5px;
    padding:10px;
}

.vote:hover{
    transition:1s;
    color:#fff;
    background-color:#036;
}
    
</style>
</head>
<body>

    <h2>一覧画面</h2><br>



    
    
<div>
    <ul>
    <?php foreach($images as $img): ?>
        <li><?php echo Asset::img($img['file_name']); ?>
            <span class="votes"><?php echo $img['votes'] ?></span>
            <input type="button" class="vote" data-id="<?php echo $img['id']; ?>" value="投票する">
        </li>
    <?php endforeach; ?>
    </ul>
</div>
<?php echo Html::anchor('vote/logout','ログアウト'); ?>
</body>
</html>