<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>アップロード画面</title>
    <style>
        body { margin:40px; }
    </style>
</head>
<body>
    アップロード画面<br>
    <?php echo Html::anchor('admin/view','一覧'); ?><br>
    <?php echo Html::anchor('admin/logout','ログアウト'); ?><br>
    <?php echo Form::open(array('action' => 'admin/upload','method' => 'post','enctype' => 'multipart/form-data')); ?>
        <?php echo Form::file('image'); ?>
        <br>
        <?php echo Form::submit('upload','アップロード'); ?>
    <?php echo Form::close(); ?>

</body>
</html>